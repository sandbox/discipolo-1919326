<?php
/**
 * @file
 * pushtape_commerce.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function pushtape_commerce_commerce_product_default_types() {
  $items = array(
    'cd' => array(
      'type' => 'cd',
      'name' => 'cd',
      'description' => 'physical cds',
      'help' => '',
      'revision' => '1',
    ),
    'digital_playlist' => array(
      'type' => 'digital_playlist',
      'name' => 'digital playlist',
      'description' => 'a product bundle of digital proucts',
      'help' => '',
      'revision' => '1',
    ),
    'digital_track' => array(
      'type' => 'digital_track',
      'name' => 'Digital Track',
      'description' => 'flac tracks for sale',
      'help' => 'upload flac files for sale here',
      'revision' => '1',
    ),
    'event_ticket' => array(
      'type' => 'event_ticket',
      'name' => 'Event ticket',
      'description' => 'a ticket for a gig',
      'help' => '',
      'revision' => '1',
    ),
  );
  return $items;
}
