core = 7.x
api = 2

projects[] = commerce
projects[] = commerce_custom_product
projects[commerce_product_bundle][version] = 1.x-dev
projects[commerce_product_bundle][patch][1489548] = https://drupal.org/files/commerce_product_bundle.module.patch

projects[] = commerce_donate
projects[] = commerce_features
projects[] = commerce_file
projects[] = select_or_other
projects[] = rules
projects[] = addressfield
